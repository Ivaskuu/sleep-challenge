import 'package:flutter/material.dart';
import 'package:tts/tts.dart';
import 'dart:async';
import 'dart:math';

class PingPongDotSixCurve extends Curve
{
  @override
  double transform(double t)
  {
    return -(8.0*pow(t, 2))/5.0 + (8.0*t)/5.0 + 3.0/5.0;
  }
} 

class UIMain extends StatefulWidget
{
  bool isSleeping = false;
  int minutes = 29;
  int seconds = 59;

  @override
  _UIMainState createState() => new _UIMainState();
}

class _UIMainState extends State<UIMain> with SingleTickerProviderStateMixin
{
  AnimationController lightIdleAnimCtrl;
  Animation<double> lightIdleAnimation;

  Timer timeTimer;

  @override
  void initState()
  {
    super.initState();

    lightIdleAnimCtrl = new AnimationController(duration: new Duration(seconds: 5), vsync: this);
    lightIdleAnimation = new CurvedAnimation(parent: lightIdleAnimCtrl, curve: new PingPongDotSixCurve());
    lightIdleAnimation.addListener(() => this.setState(() {}));
    lightIdleAnimation.addStatusListener((AnimationStatus status) {});
    lightIdleAnimCtrl.repeat();
  }

  @override
  void dispose()
  {
    lightIdleAnimCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: new SizedBox.expand
      (
        child: new Stack
        (
          alignment: Alignment.center,
          children: <Widget>
          [
            backgroundColor(),
            bedImage,
            lightImage(),
            slider()
          ],
        )
      ),
    );
  }

  void sleepTrigger()
  {
    // Reset state
    this.setState(() => widget.isSleeping = !widget.isSleeping);

    if(widget.isSleeping)
    {
      widget.minutes = 29;
      widget.seconds = 59;

      timeTimer = new Timer.periodic(new Duration(seconds: 1), (_) => setState(()
      {
        widget.seconds--;
        if(widget.seconds < 0)
        {
          widget.minutes--;
          widget.seconds = 59;

          if(widget.minutes < 0)
          {
            sleepTrigger();
            timeTimer.cancel();
          }
        }
      }));
    }
    else
    {
      timeTimer.cancel();
    }

    // Get phone lang and set the Tts engine and wikipedia
    // Get a random wikipedia article
    // Read it with the Tts engine
  }

  Widget backgroundColor()
  {
    if(widget.isSleeping)
    {
      /// BackgroundOff
      return new Container(color: new Color.fromRGBO(4, 14, 44, 1.0));
    }
    else
    {
      /// BackgroundOn
      return new Container
      (
        decoration: new BoxDecoration
        (
          gradient: new LinearGradient
          (
            colors: [ new Color.fromRGBO(4, 14, 44, 1.0), new Color.fromRGBO(11, 30, 88, 1.0) ],
            begin: new Alignment(0.0, 0.0),
            end: new Alignment(1.0, 1.0)
          ),
        ),
      );
    }
  }

  Widget bedImage = new Container
  (
    decoration: new BoxDecoration
    (
      image: new DecorationImage
      (
        image: new AssetImage('res/bed.png'),
        fit: BoxFit.fitWidth
      )
    ),
  );

  Widget lightImage()
  {
    if(!widget.isSleeping)
    {
      return new Container
      (
        child: new Image.asset
        (
          'res/light.png',
          fit: BoxFit.fitWidth,
          color: new Color.fromRGBO(255, 255, 94, lightIdleAnimation.value),
        ),
      );
    }
    else return new Container();
  }

  Widget slider()
  {
    if(!widget.isSleeping)
    {
      return new Align
      (
        alignment: Alignment.bottomCenter,
        child: new Container
        (
          margin: new EdgeInsets.all(32.0),
          child: new SizedBox.fromSize
          (
            size: new Size.fromHeight(96.0),
            child: new Material
            (
              borderRadius: new BorderRadius.circular(10.0),
              color: new Color.fromRGBO(110, 114, 184, 0.2),
              child: new Container
              (
                margin: new EdgeInsets.all(12.0),
                child: new SizedBox // Thumb
                (
                  width: 20.0,
                  child: new Material
                  (
                    borderRadius: new BorderRadius.circular(10.0),
                    color: new Color.fromRGBO(110, 114, 184, 1.0),
                    elevation: 8.0,
                    child: new InkWell
                    (
                      onTap: () => sleepTrigger(),
                      child: new Image.asset('res/moon.png'),
                    )
                  )
                ),
              ),
            ),
          )
        ),
      );
    }
    else
    {
      return new Align
      (
        alignment: Alignment.bottomCenter,
        child: new Container
        (
          margin: new EdgeInsets.all(32.0),
          child: new SizedBox.fromSize
          (
            size: new Size.fromHeight(96.0),
            child: new Material
            (
              borderRadius: new BorderRadius.circular(10.0),
              color: new Color.fromRGBO(110, 114, 184, 0.2),
              child: new Container
              (
                margin: new EdgeInsets.all(12.0),
                child: new SizedBox // Thumb
                (
                  width: 20.0,
                  child: new Material
                  (
                    borderRadius: new BorderRadius.circular(10.0),
                    color: new Color.fromRGBO(110, 114, 184, 1.0),
                    elevation: 0.0,
                    child: new InkWell
                    (
                      onTap: () => sleepTrigger(),
                      child: new Center
                      (
                        child: new Text
                        (
                          widget.seconds < 10 // Show a leading 0
                          ? 'You will fall asleep in... ${widget.minutes}:0${widget.seconds} :)'
                          : 'You will fall asleep in... ${widget.minutes}:${widget.seconds} :)',
                          style: new TextStyle(color: new Color.fromRGBO(255, 255, 255, 0.5), fontWeight: FontWeight.w600)
                        )
                      )
                    )
                  )
                ),
              ),
            ),
          )
        ),
      );
    }
  }
}